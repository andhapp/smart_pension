# Log Parser and Analyser

## How to run

1. Clone the repository
2. Install dependencies via Bundler:

```
bundle install --path vendor
```
3. To run the specs:
```
bundle exec rspec
```
4. To run the parser_and_analyser:
```
./bin/parser_and_analyser.rb ./data/webserver.log
```

I changed it slightly to create two different classes, one for parsing and one for analysing. Having
them in the same class felt like it doing 2 things and breaking Single Responsibility Principle. Hence,
the name of the script reflects the same.

## Possible improvements

1. Introduce a class that knows how to output the data and could take an IO object (File, $stdout, etc.) and write to it.
2. Introduce rubocop
