#!/usr/bin/env ruby

require 'set'
require 'byebug'

require_relative '../lib/smart_pension'

raise StandardError.new('Please provide webserver lot path as argument') if ARGV.empty?

webserver_log_path = ARGV[0]

log_parser = SmartPension::LogParser.new(
  log_filepath: webserver_log_path,
  visit_klass: SmartPension::Visit
)
log_parser.process!

analyser = SmartPension::LogAnalyser.new(log_parser: log_parser)

puts '=' * 60
puts '1. List of webpages with most page views (ordered)'
puts '=' * 60
analyser.analyse_all_visits.each do |all_visits_array|
  puts "#{all_visits_array.first} #{all_visits_array.last.count} visits"
end

puts

puts '=' * 60
puts '2. List of webpages with most unique page views (ordered)'
puts '=' * 60
analyser.analyse_all_unique_visits.each do |all_visits_array|
  puts "#{all_visits_array.first} #{all_visits_array.last.count} unique visits"
end
