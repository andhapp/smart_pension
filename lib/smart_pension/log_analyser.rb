module SmartPension
  class LogAnalyser
    def initialize(log_parser:)
      @log_parser = log_parser
    end

    def analyse_all_visits
      analyse(@log_parser.all_visits)
    end

    def analyse_all_unique_visits
      analyse(@log_parser.all_unique_visits)
    end

    private

    def analyse(data)
      data.group_by do |visit|
        visit.path
      end.sort_by do |path, visits|
        visits.length
      end.reverse!
    end
  end
end
