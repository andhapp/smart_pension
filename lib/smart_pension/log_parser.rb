module SmartPension
  class LogParser
    attr_reader :all_visits, :all_unique_visits

    def initialize(log_filepath:, visit_klass:)
      @log_filepath = log_filepath
      @visit_klass = visit_klass
      @all_visits = []
      @all_unique_visits = Set.new
    end

    def process!
      File.foreach(@log_filepath) do |raw_request|
        path, ip_address = raw_request.split(' ')

        visit = @visit_klass.send(
          :create_from_path_and_ip_address,
          path: path,
          ip_address: ip_address
        )

        @all_visits << visit
        @all_unique_visits << visit
      end
    end
  end
end
