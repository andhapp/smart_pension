module SmartPension
  class Page
    attr_reader :path

    def initialize(path:)
      @path = path
    end
  end
end
