require 'forwardable'

module SmartPension
  class Visit
    extend Forwardable

    attr_reader :page, :visitor

    def_delegators :@page, :path
    def_delegators :@visitor, :ip_address

    def self.create_from_path_and_ip_address(path:, ip_address:)
      page = Page.new(path: path)
      visitor = Visitor.new(ip_address: ip_address)

      new(page: page, visitor: visitor)
    end

    def initialize(page:, visitor:)
      @page = page
      @visitor = visitor
    end

    def eql?(other)
      page.path == other.path &&
        visitor.ip_address == other.ip_address
    end

    def hash
      [page.path, visitor.ip_address].hash
    end
  end
end
