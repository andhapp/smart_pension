# frozen_string_literal: true

describe SmartPension::LogAnalyser do
  subject(:log_analyser) do
    described_class.new(log_parser: log_parser)
  end

  let(:log_parser) do
    SmartPension::LogParser.new(log_filepath: log_filepath, visit_klass: visit_klass)
  end
  let(:log_filepath) do
    File.expand_path('../../../files/webserver_log_sample.log', __FILE__)
  end
  let(:visit_klass) { SmartPension::Visit }

  let(:path1) { '/home/1' }
  let(:visit1) do
    SmartPension::Visit.create_from_path_and_ip_address(path: path1, ip_address: '8.8.8.8')
  end
  let(:path2) { '/home/2' }
  let(:visit2) do
    SmartPension::Visit.create_from_path_and_ip_address(path: path2, ip_address: '123.1.1.1')
  end
  let(:path3) { '/about/1' }
  let(:visit3) do
    SmartPension::Visit.create_from_path_and_ip_address(path: path3, ip_address: '12.34.5.98')
  end

  before { log_parser.process! }

  describe '#analyse_all_visits' do
    it 'analyses all_visits data and return a multidimensional array' do
      expect(log_analyser.analyse_all_visits).to eql([
        [path1, [visit1, visit1]],
        [path3, [visit3]],
        [path2, [visit2]]
      ])
    end
  end

  describe '#analyse_all_unique_visits' do
    it 'analyses all_visits data and return a multidimensional array' do
      expect(log_analyser.analyse_all_unique_visits).to eql([
        [path3, [visit3]],
        [path2, [visit2]],
        [path1, [visit1]]
      ])
    end
  end
end
