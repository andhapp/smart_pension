# frozen_string_literal: true

describe SmartPension::LogParser do
  subject(:log_parser) do
    described_class.new(log_filepath: log_filepath, visit_klass: visit_klass)
  end

  let(:log_filepath) do
    File.expand_path('../../../files/webserver_log_sample.log', __FILE__)
  end
  let(:visit_klass) { SmartPension::Visit }
  let(:visit1) do
    SmartPension::Visit.create_from_path_and_ip_address(path: '/home/1', ip_address: '8.8.8.8')
  end
  let(:visit2) do
    SmartPension::Visit.create_from_path_and_ip_address(path: '/home/2', ip_address: '123.1.1.1')
  end
  let(:visit3) do
    SmartPension::Visit.create_from_path_and_ip_address(path: '/about/1', ip_address: '12.34.5.98')
  end

  describe '#process' do
    it 'calculates "all_visits" and "all_unique_visits"' do
      log_parser.process!

      aggregate_failures do
        expect(log_parser.all_visits).to eql([visit1, visit2, visit1, visit3])
        expect(log_parser.all_unique_visits).to eql(Set.new([visit1, visit2, visit3]))
      end
    end
  end
end
