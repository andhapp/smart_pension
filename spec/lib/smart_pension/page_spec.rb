# frozen_string_literal: true

RSpec.describe SmartPension::Page do
  describe '#initialize' do
    subject(:page) { described_class.new(path: path) }

    let(:path) { '/about/2' }

    it 'sets the "path" on initialization' do
      expect(page.path).to eq(path)
    end
  end
end
