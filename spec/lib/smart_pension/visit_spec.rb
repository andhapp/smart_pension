# frozen_string_literal: true

describe SmartPension::Visit do
  subject(:visit) do
    described_class.create_from_path_and_ip_address(
      path: path,
      ip_address: ip_address
    )
  end

  let(:path) { '/home' }
  let(:ip_address) { '127.0.0.1' }

  describe '.create_from_path_and_ip_address' do
    it 'creates and returns a new instance of SmartPension::Visit' do
      aggregate_failures do
        expect(visit).to be_instance_of(described_class)
        expect(visit.page).to be_instance_of(SmartPension::Page)
        expect(visit.visitor).to be_instance_of(SmartPension::Visitor)
      end
    end
  end

  describe '#initialize' do
    subject(:visit) do
      described_class.new(page: page, visitor: visitor)
    end

    let(:page) { SmartPension::Page.new(path: path) }
    let(:visitor) { SmartPension::Visitor.new(ip_address: ip_address) }

    it 'sets "page" and "visitor" on initialization' do
      aggregate_failures do
        expect(visit.page).to eq(page)
        expect(visit.visitor).to eq(visitor)
      end
    end
  end

  describe 'delegation' do
    it 'delegates "path" to "page" instance' do
      expect(visit.path).to eq(path)
    end

    it 'delegates "ip_address" to "visitor" instance' do
      expect(visit.ip_address).to eq(ip_address)
    end
  end

  describe '#eql?' do
    context 'when "path" and "ip_address" for both instances are same' do
      let(:other_visit) do
        described_class.create_from_path_and_ip_address(
          path: path,
          ip_address: ip_address
        )
      end

      it 'is equal to other instance' do
        expect(visit).to eql(other_visit)
      end
    end

    context 'when "path" for both instances are different' do
      let(:other_visit) do
        described_class.create_from_path_and_ip_address(
          path: '/about/1',
          ip_address: ip_address
        )
      end

      it 'is not equal to other instance' do
        expect(visit).not_to eql(other_visit)
      end
    end

    context 'when "ip_address" for both instances are different' do
      let(:other_visit) do
        described_class.create_from_path_and_ip_address(
          path: path,
          ip_address: '0.0.0.0'
        )
      end

      it 'is not equal to other instance' do
        expect(visit).not_to eql(other_visit)
      end
    end
  end

  describe '#hash' do
    it 'calculates hash using "path" and "ip_address"' do
      expect(visit.hash).to eq([path, ip_address].hash)
    end
  end
end
