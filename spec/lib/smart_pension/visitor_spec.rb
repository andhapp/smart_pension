# frozen_string_literal: true

RSpec.describe SmartPension::Visitor do
  describe '#initialize' do
    subject(:visitor) { described_class.new(ip_address: ip_address) }

    let(:ip_address) { '8.8.8.8' }

    it 'sets the "ip_address" on initialization' do
      expect(visitor.ip_address).to eq(ip_address)
    end
  end
end
